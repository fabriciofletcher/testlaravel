# Database

## Configuring
Just update the file in `app/config/database.php` accordingly.
For the learning purpose, we will use the sqlite, so modify the line
    
    'default' => 'mysql'

to

    'default' => 'sqlite'

The local database file will be create at `app/database/*.sqlite`

## Migration: Creating and evolving DB schema
The migraton is a way of creating and evolving database schema using php script.

the migration files are created by using the artisan tool:

    $php artisan migrate:make create_users_table

The above command will generate the migration file and place in `app/database/migrations`
You can use the [Taskmator](https://github.com/altenia/taskmator) tool to generate the table crate migration scripts from a json schema.

In order to execute the migration, do:

    php artisan migrate

Check that the tables were created. You may use the [SQLite Database Browser](http://sourceforge.net/projects/sqlitebrowser/).
* Remember that SQlite is an embedded server and if an application is using the file, it retains the lock and no other application will be able to connect.

## Seeding: Populating DB with initial records
You will create a per-table seeder script file and place in `app/database/seeder/` folder.
The naming convetion is <Tablename>TableSeeder

You can also create a sample seeder file using the Taskmator.

Once the per-table seeder script file is crated, add a line in the `app/database/seeder/DatabaseSeeder.php` that runs the seeder:

    $this->call('UserTableSeeder');

And to run the seeder:
    
    $php artisan db:seed

It is also possible to rollback and seed:

    $php artisan migrate:refresh --seed
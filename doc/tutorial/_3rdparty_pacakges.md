# Integration with 3rd party libraries

## Bootstrap

There are two ways of integrating Bootstap.
Manually: you can go to [Boostrap](http://getbootstrap.com/getting-started/), download and copy the necessary files under `/public` directory.
By Composer: you can add dependency in the composer.json file

	"require": {
      "laravel/framework": "4.0.*",
      "twitter/bootstrap": "*"
	},

and install it by executing `composer update`

Then go to `<project>/vendor/twitter/bootstrap` directory and execute
`npm install` (You will require nodejs or npmjs installed in your computer)



## REFERENCE
http://net.tutsplus.com/tutorials/php/authentication-with-laravel-4/
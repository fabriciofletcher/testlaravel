<?php

/** 
 * @Tutorial - To seed a table
 *             Remember to add this class to the DatabaseSeeder.php as:
 *             $this->call('UsersTableSeeder');
 */

/** 
 * @Tutorial - To seed a table
 */
class UsersTableSeeder extends Seeder {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function run()
	{
	    //DB::table('users')->delete();
	    $users = [
	        [
			'domain_id' => 'domain_id',
			'created_dt' => new DateTime,
			'updated_dt' => new DateTime,
			'updated_counter' => 1,
			'uuid' => 'uuid',
			'original_domain_id' => 'original_domain_id',
			'id' => 'id',
			'password' => 'password',
			'first_name' => 'first_name',
			'middle_name' => 'middle_name',
			'last_name' => 'last_name',
			'lc_name' => 'lc_name',
			'display_name' => 'display_name',
			'bdate' => new DateTime,
			'phone' => 'phone',
			'email' => 'email',
			'permalink' => 'permalink',
			'activation_code' => 'activation_code',
			'security_question' => 'security_question',
			'security_answer' => 'security_answer',
			'login_fail_counter' => 1,
			'status' => 1,
			'default_lang_cd' => 'default_lang_cd',
			'timezone' => 'timezone',
			'expiry_dt' => new DateTime,
			'active_project_sid' => 1,
			'type' => 'type',
			'params_text' => 'params_text'
            ]
        ];
        DB::table('users')->insert($users);

	}
}
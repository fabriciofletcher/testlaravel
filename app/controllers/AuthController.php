<?php

/**
 * @Tutorial
 * Auth Controller for authetnication
 * Notice app/routes.php maps using Route::controller
 * The method is prefixed by the HTTP method name
 *
 * Taken from: http://net.tutsplus.com/tutorials/php/authentication-with-laravel-4/
 */
class AuthController extends BaseController { 

	/**
	 * If you look at the BaseController, you will notice that the 
	 * setupLayout() method will 
	 */
	protected $layout = 'layouts.default';

	/**
	 * @Tutorial
	 * Filter for guaring agains CSFR, and for protecting private pages. 
	 */
	public function __construct() {
	   $this->beforeFilter('csrf', array('on'=>'post'));
	   $this->beforeFilter('auth', array('only'=>array('getDashboard')));
	}

	/**
	 * Show Login form
	 * Note: You must customize the defualt behavior of redirecting to /login
	 * by modifying the file app/filters.php with 'auth' filter that returns
	 * `/auth/signin` instead of `/login`
	 */
	public function getSignin() {
		$this->layout->content = View::make('auth.signin');
	}

	/**
	 * Do Logout
	 */
	public function getSignout() {
		Auth::logout();
   		return Redirect::to('auth/signin')
   			->with('message', 'Your have logged out!');
	}

	/**
	 * Do Login
	 */
	public function postSignin()
	{
		$attempt_input = array('id'=>Input::get('id'), 'password'=>Input::get('password'));
		if (Auth::attempt($attempt_input)) {
			Log::debug('Signin attemp failed.');
			return Redirect::to('auth/dashboard')->with('message', 'You are now logged in!');
		} else {
			Session::flash('message', 'Your username/password combination was incorrect!');
		   return Redirect::to('auth/signin')
		      ->withInput();
		}
	}

	/**
	 * Some protected content
	 */
	public function getDashboard() {
    	return Redirect::to('users');
	}
}

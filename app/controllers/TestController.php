<?php

/**
 * @Tutorial
 * Test Controller for tutorial purpose
 */
class TestController extends BaseController { 

	/**
	 * If you look at the BaseController, you will notice that the 
	 * setupLayout() method will 
	 */
	protected $layout = 'layouts.default';

	/**
	 * A super simple controller action that returns a text
	 */
	public function showHelloWorld()
	{
		return 'Hello World';
	}

	/**
	 * A fancier action with layout and content view
	 */
	public function showFancyHello()
	{
		$messages = array("Hello World", "This is Laravel in Action");
		$this->layout->content = View::make('test.hello')
			->with("messages", $messages);
	}
}
